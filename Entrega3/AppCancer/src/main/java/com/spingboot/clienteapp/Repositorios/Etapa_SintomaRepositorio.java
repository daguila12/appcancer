package com.spingboot.clienteapp.Repositorios;
import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;


public interface Etapa_SintomaRepositorio extends JpaRepository<Etapa_Sintoma, Long>  {
	Etapa_Sintoma findById(long id);
}
