package com.spingboot.clienteapp.Repositorios;

import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteRepositorio extends JpaRepository<Paciente, Long>  {
	Paciente findById(long id);
}
