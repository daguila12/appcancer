package com.spingboot.clienteapp.Repositorios;

import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {
	Usuario findById(long id);
}
