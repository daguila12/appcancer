package com.spingboot.clienteapp.Repositorios;
import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;


public interface EtapaRepositorio extends JpaRepository<Etapa, Long>  {
	Etapa findById(long id);
}
