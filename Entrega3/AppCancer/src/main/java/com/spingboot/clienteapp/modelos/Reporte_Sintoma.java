package com.spingboot.clienteapp.modelos;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "Reporte_Sintoma")
public class Reporte_Sintoma {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private long IdReporte;
	private long IdSintoma;
	private int severidad;
	
	
	
	public Reporte_Sintoma() {};
	
	public Reporte_Sintoma(long id, long idReporte, long idSintoma, int severidad) {
		super();
		this.id = id;
		IdReporte = idReporte;
		IdSintoma = idSintoma;
		this.severidad = severidad;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdReporte() {
		return IdReporte;
	}
	public void setIdReporte(long idReporte) {
		IdReporte = idReporte;
	}
	public long getIdSintoma() {
		return IdSintoma;
	}
	public void setIdSintoma(long idSintoma) {
		IdSintoma = idSintoma;
	}
	public int getSeveridad() {
		return severidad;
	}
	public void setSeveridad(int severidad) {
		this.severidad = severidad;
	}
	
	

}
