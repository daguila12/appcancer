
package com.spingboot.clienteapp;

import com.spingboot.clienteapp.modelos.*;

import com.spingboot.clienteapp.Repositorios.*;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import java.util.Base64;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.json.*;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class RouterController {
//##################//	
//   Repositorios   //	
//##################//	
	
	@Autowired
	UsuarioRepositorio urepo;
	
	@Autowired
	PreguntaPacienteRepositorio pprepo;
	
	@Autowired
	PacienteRepositorio prepo;
	
	@Autowired
	UsuarioRepositorio trepo;
	
	@Autowired
	EtapaRepositorio erepo;
	
	@Autowired
	SintomaRepositorio srepo;
	
	@Autowired
	Etapa_SintomaRepositorio esrepo;
	
	@Autowired
	ReporteRepositorio rrepo;
	
	@Autowired
	ReporteSintomaRepositorio rsrepo;

	
//###############//
//  Mapeos       //
//###############//
	@GetMapping("/login")
    public String login(Model modelo) {
    List<PreguntaPaciente> preguntas =new ArrayList<PreguntaPaciente>();
    for(PreguntaPaciente p : pprepo.findAll()) {
    	if(p.getRespuesta().equals("")) {
    		preguntas.add(p);
    	}
    }

    modelo.addAttribute("preguntas",preguntas);
	return "loginForm";
   }
	@GetMapping("/")
       public String index() {
           return "index";
       }

	@GetMapping("/history")
	public String history(Model modelo) {
		List<PreguntaPaciente> respuestas =new ArrayList<PreguntaPaciente>();
	    for(PreguntaPaciente p : pprepo.findAll()) {
	    	if(!p.getRespuesta().equals("")) {
	    		respuestas.add(p);
	    	}
	    }	
	    modelo.addAttribute("respuestas",respuestas);
		return "menu";
	}
	
	@GetMapping("/reportarSintomas/{idpaciente}/{idsintoma}/{idreporte}/{severidad}")
	public String reportarSintomasRespuesta(Model modelo, @PathVariable(value="idpaciente") long id,@PathVariable(value="idsintoma") long idsintoma
   , @PathVariable(value="idreporte") long idreporte,@PathVariable(value="severidad") int severidad) 
	{
		Reporte_Sintoma report = new Reporte_Sintoma();
		report.setIdReporte(idreporte);
		report.setIdSintoma(idsintoma);
		report.setSeveridad(severidad);
		//Guardando solo la ultima respuesta en cada sintoma
		for(Reporte_Sintoma rs : rsrepo.findAll()) {
			if ((rs.getIdReporte() == report.getIdReporte()) && (rs.getIdSintoma()==report.getIdSintoma()))
			{
			    rsrepo.delete(rs);
			}
		}
	
		rsrepo.save(report);
		int encontrado = 0;
		//Borrando 
		for(Reporte r : rrepo.findAll()) {
			encontrado = 0;
			
			for(Reporte_Sintoma rs : rsrepo.findAll()) {
			if(r.getIdReporte() == rs.getIdReporte())  
			encontrado = 1;
			}
			
			if(encontrado == 0)
				rrepo.delete(r);
		}
		
		Reporte reporte = rrepo.findById(idreporte);
		
		modelo.addAttribute("reporte",reporte);
		Paciente paciente = prepo.findById(id);
		Usuario  usuario = urepo.findById(id);
		modelo.addAttribute("usuario",usuario);
		
	    Etapa etapa = erepo.findById(paciente.getIdEtapa());
	    modelo.addAttribute("etapa",etapa);
	    
		List<Sintoma> sintomas =  new ArrayList<Sintoma>();
		for(Etapa_Sintoma e : esrepo.findAll()) {
			if(e.getIdEtapa() == etapa.getIdEtapa()) {
				sintomas.add(srepo.findById(e.getIdSintoma()));
			}
		}
		modelo.addAttribute("sintomas",sintomas);
	    return "reportarSintoma";
	}
	
	
	
	@RequestMapping(value = "/SaveReportarSintomas", method = RequestMethod.POST)
	public String saveReportarSintomas(@RequestBody String obj) {
		JsonParser springParser = JsonParserFactory.getJsonParser();
		String result = null;
	    try{
	      result = URLDecoder.decode(obj, "UTF-8");
	    }
	    // This exception should never occur
	    catch (UnsupportedEncodingException e){
	      result = obj;  
	    }
	    
		Map<String, Object> map = springParser.parseMap(result);
		String[] pac = map.get("idpaciente").toString().split("-", -1);
		String[] etp = map.get("idetapa").toString().split("-", -1);
		String[] sintomas = map.get("sin").toString().split("-", -1);

		//{"idpaciente":["1"],"idetapa":["1"],"sin":["4,1","4,2","4,3"]}
	
		
		Reporte reporte = new Reporte();
		reporte.setIdPaciente(Long.parseLong(pac[0]));
		rrepo.save(reporte);

		String[] aux;

		for(String sin : sintomas) {
			aux = sin.toString().split(",", -1);
			Reporte_Sintoma reporteSintoma = new Reporte_Sintoma();
			reporteSintoma.setIdSintoma(Long.parseLong(aux[1]));
			reporteSintoma.setIdReporte(reporte.getIdReporte());
			reporteSintoma.setSeveridad(Integer.parseInt(aux[0]));
			rsrepo.save(reporteSintoma);
		}
		
		
		
		
		return "redirect:/reportarSintomas/"+pac[0];
	}
	
	@GetMapping("/reportarSintomas/{idpaciente}")
	public String reportarSintomas(Model modelo, @PathVariable(value="idpaciente") long id) {
		/*Reporte reporte = new Reporte();
		reporte.setIdPaciente(id);
		rrepo.save(reporte);
		modelo.addAttribute("reporte",reporte);*/
		Paciente paciente = prepo.findById(id);
		
		Usuario usuario = urepo.findById(id);
		modelo.addAttribute("usuario",usuario);
		
	    Etapa etapa = erepo.findById(paciente.getIdEtapa());
	    modelo.addAttribute("etapa",etapa);
	    
		List<Sintoma> sintomas =  new ArrayList<Sintoma>();
		for(Etapa_Sintoma e : esrepo.findAll()) {
			if(e.getIdEtapa() == etapa.getIdEtapa()) {
				sintomas.add(srepo.findById(e.getIdSintoma()));
			}
		}
		modelo.addAttribute("sintomas",sintomas);
	    return "reportarSintoma";
	}
	
	/*@GetMapping("/reportarSintomas")
	public String reportarSintomas(Model modelo) {
	    return "reportarSintoma";
	}*/
	
	
	
	@GetMapping("/preguntaView/{id}")
	public String viewPregunta(Model modelo, @PathVariable(value="id") long id) {
		modelo.addAttribute("pregunta",pprepo.findById(id));
		return "preguntaView";
	}
	
	
	//############//	
	//Respuestas  //
	//############//	
	@PostMapping("/preguntaView/{id}")
	public String viewPregunta(Model modelo,Respuesta Respuesta, @PathVariable(value="id") long id ) {
		PreguntaPaciente temp;
	    for(PreguntaPaciente p : pprepo.findAll()) {
	    	if(p.getNumero()==id) {
	    		p.setRespuesta(Respuesta.getRespuesta());
	    		temp=p;
	    		pprepo.delete(p);
	    		pprepo.save(temp);
	    	}
	    }
		return "index";
	}
	
	
	
	//############//	
	//Listado Reportes de Síntomas//
	//############//	
	@GetMapping("/reportesSintomas")
	public String viewReportesSintomas(Model modelo) {
		List<Reporte> rep_array = new ArrayList<Reporte>();
		List<Paciente> pac_array = new ArrayList<Paciente>();
		//se obtienen todos los reportes en BD
		for(Reporte r : rrepo.findAll()) {
			/**ClasificacionProfesional en BD inicializa con el valor -1 
			hasta que el profesional de la salud clasifica el reporte según su severidad**/
			if(r.getClasificacionProfesional() == -1) {
				//INFO REPORTE + INFO PACIENTE
				for(Paciente p : prepo.findAll()) {
					if( p.getIdUsuario() == r.getIdPaciente() ) {
						//Los indices de ambos arrays son correspondiente entre sí
						//por ejemplo la información de paciente de rep_array[i] está en pac_array[i]
						rep_array.add(r);
						pac_array.add(p);
					}
				}
			}
		}
		modelo.addAttribute("reportes",rep_array);
	    modelo.addAttribute("pacientes",pac_array);

		
		return "reportesSintomas";
	}
	
	
	//############//	
	//Información Reporte de Síntomas//
	//############//	
	@GetMapping("/getReporteSintomaData/{idreporte}")
	public String viewReporteSintoma(Model modelo, @PathVariable(value="idreporte") long idreporte) {
		System.out.println("reporte sintoma data"+ idreporte);
		Reporte rep = rrepo.findById(idreporte);
		Paciente pac;
		List<Sintoma> sintomas = new ArrayList<Sintoma>();
		List<Reporte_Sintoma> rep_sintomas = new ArrayList<Reporte_Sintoma>();
		//se usa para reagrupar los elementos de rep_sintomas para ordenados igual que sintomas
		List<Reporte_Sintoma> rep_sintomas2 = new ArrayList<Reporte_Sintoma>();
		
		for(Paciente p : prepo.findAll()) {
			if( p.getIdUsuario() == rep.getIdPaciente() ) {
				pac = p;
				modelo.addAttribute("paciente", pac);
			}
		}
		for(Reporte_Sintoma rs : rsrepo.findAll()){
			if( rs.getIdReporte() == rep.getIdReporte() ) {
				System.out.println("se obtine reporte_sintoma");
				//Se seleccionan solo los Reporte_Sintoma con IdReporte rep.getIdReporte()
				rep_sintomas.add(rs);
			}
		}
		for( Reporte_Sintoma rs2 : rep_sintomas) {
			for(Sintoma s : srepo.findAll()){
				if( s.getIdSintoma() == rs2.getIdSintoma() ) {
					rep_sintomas2.add(rs2);
					sintomas.add(s);
				}
			}
		}
		modelo.addAttribute("reporte", rep);
		modelo.addAttribute("sintomas", sintomas);
		//aquí está la severidad del sintoma
		modelo.addAttribute("rep_sintomas", rep_sintomas2);
		String[] sev = {"Muy Bien", "Bien", "Normal", "Mal", "Muy Mal"};
		modelo.addAttribute("sev", sev);
		
		return "modalReporteSintomas";
	}
	
	
	
	//############//	
	//controlador para updatear la tabla reporte de sintomas (clasificación de profesional)//
	//############//	
	@RequestMapping(value = "/editReporteSintomas", method = RequestMethod.POST)
	public String viewReporteSintoma(@RequestBody String obj) {
		JsonParser springParser = JsonParserFactory.getJsonParser();
		String result = null;
	    try{
		  result = URLDecoder.decode(obj, "UTF-8");
	    }
	    // This exception should never occur
	    catch (UnsupportedEncodingException e){
	      result = obj;
	    }
	    
	    
	    Map<String, Object> map = springParser.parseMap(result);
		//map = {clas int, msg string, idreporte int }
		//map = {clas: 2, msg: 'esta empeorando', idreporte: 10}
	    //actualizar columna de tabla reporte (ClasificacionProfesional INT DEFAULT -1, ComentarioProfesional String DEFAULT null) 
	    //obtener datos FALTA
		Reporte r = rrepo.findById(Integer.parseInt(map.get("idreporte").toString()));
		r.setClasificacionProfesional(Integer.parseInt(map.get("clas").toString()));
		r.setComentarioProfesional(map.get("msg").toString());
		rrepo.save(r);
		
		return "redirect:/reportesSintomas";
	}
}	
