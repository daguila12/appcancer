package com.spingboot.clienteapp.Repositorios;

import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PreguntaPacienteRepositorio extends JpaRepository<PreguntaPaciente, Long> {
	PreguntaPaciente findById(long id);
}
