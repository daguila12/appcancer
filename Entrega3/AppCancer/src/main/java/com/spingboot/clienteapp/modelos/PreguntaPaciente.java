package com.spingboot.clienteapp.modelos;

import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "pregunta")
public class PreguntaPaciente{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idpregunta;
	
	private String NombreCompleto;
	private String Pregunta;
	private Long numero;

	private String Respuesta;
	public PreguntaPaciente() {}
	public PreguntaPaciente(Long numero,String nombreCompleto, String pregunta ) {
		super();
		NombreCompleto = nombreCompleto;
		Pregunta = pregunta;
		Respuesta="";
		this.numero = numero;
	}
	
	public PreguntaPaciente(Long numero,String nombreCompleto, String pregunta ,String Respuesta) {
		super();
		this.Respuesta = Respuesta;
		NombreCompleto = nombreCompleto;
		Pregunta = pregunta;
		this.numero = numero;
	}
	public String getRespuesta() {
		return Respuesta;
	}
	public void setRespuesta(String respuesta) {
		Respuesta = respuesta;
	}
	
	public String getNombreCompleto() {
		return NombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		NombreCompleto = nombreCompleto;
	}
	public String getPregunta() {
		return Pregunta;
	}
	public void setPregunta(String pregunta) {
		Pregunta = pregunta;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	
	
	}
