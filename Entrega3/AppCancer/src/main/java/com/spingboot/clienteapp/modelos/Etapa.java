package com.spingboot.clienteapp.modelos;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "Etapa")
public class Etapa {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long IdEtapa;
	private long IdTratamiento;
	private String NombreEtapa;
	
	public Etapa() {};
	
	public Etapa(long idEtapa, long idTratamiento, String nombreEtapa) {
		super();
		IdEtapa = idEtapa;
		IdTratamiento = idTratamiento;
		NombreEtapa = nombreEtapa;
	}
	public long getIdEtapa() {
		return IdEtapa;
	}
	public void setIdEtapa(long idEtapa) {
		IdEtapa = idEtapa;
	}
	public long getIdTratamiento() {
		return IdTratamiento;
	}
	public void setIdTratamiento(long idTratamiento) {
		IdTratamiento = idTratamiento;
	}
	public String getNombreEtapa() {
		return NombreEtapa;
	}
	public void setNombreEtapa(String nombreEtapa) {
		NombreEtapa = nombreEtapa;
	}
	
	
}
