package com.spingboot.clienteapp.Repositorios;
import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TratamientoRepositorio extends JpaRepository<Tratamiento, Long> {
	PreguntaPaciente findById(long id);
}
