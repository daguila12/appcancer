package com.spingboot.clienteapp.Repositorios;
import com.spingboot.clienteapp.modelos.*;

import javax.persistence.Column;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ReporteRepositorio extends JpaRepository<Reporte, Long>  {
	Reporte findById(long id);
	
}