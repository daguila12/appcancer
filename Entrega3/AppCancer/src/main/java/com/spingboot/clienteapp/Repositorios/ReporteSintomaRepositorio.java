package com.spingboot.clienteapp.Repositorios;
import com.spingboot.clienteapp.modelos.*;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ReporteSintomaRepositorio extends JpaRepository<Reporte_Sintoma, Long>  {
	
	Reporte_Sintoma findById(long id);
}
