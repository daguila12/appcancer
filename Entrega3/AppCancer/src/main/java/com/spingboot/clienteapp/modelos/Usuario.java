package com.spingboot.clienteapp.modelos;


import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.InheritanceType;

@Entity
@Table(name = "Usuario")
@Inheritance(strategy=InheritanceType.JOINED)
public class Usuario {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long IdUsuario;
	private String correo;
    private String NombreUsuario;
    private String contrasena;
  
    public Usuario() {};
    
	public Usuario(long idUsuario, String correo, String nombreUsuario, String contrasena) {
		super();
		IdUsuario = idUsuario;
		this.correo = correo;
		NombreUsuario = nombreUsuario;
		this.contrasena = contrasena;

	}
	public long getIdUsuario() {
		return IdUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		IdUsuario = idUsuario;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombreUsuario() {
		return NombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		NombreUsuario = nombreUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

}
