package com.spingboot.clienteapp.Repositorios;
import com.spingboot.clienteapp.modelos.*;

import org.springframework.data.jpa.repository.JpaRepository;


public interface SintomaRepositorio extends JpaRepository<Sintoma, Long>  {
	Sintoma findById(long id);
}

