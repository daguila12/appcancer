package com.spingboot.clienteapp.modelos;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "Tratamiento")
public class Tratamiento {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long IdTratamiento;
	private String NombreEtapa;
	
	public Tratamiento() {};
	
	public Tratamiento(long idTratamiento, String nombreEtapa) {
		super();
		IdTratamiento = idTratamiento;
		NombreEtapa = nombreEtapa;
	}
	public long getIdTratamiento() {
		return IdTratamiento;
	}
	public void setIdTratamiento(long idTratamiento) {
		IdTratamiento = idTratamiento;
	}
	public String getNombreEtapa() {
		return NombreEtapa;
	}
	public void setNombreEtapa(String nombreEtapa) {
		NombreEtapa = nombreEtapa;
	}
	

}
