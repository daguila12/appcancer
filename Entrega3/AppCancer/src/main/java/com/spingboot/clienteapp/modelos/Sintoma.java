package com.spingboot.clienteapp.modelos;
import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "Sintoma")
public class Sintoma {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idSintoma;
	private String sintoma;
	
	
	public Sintoma() {};
	
	public Sintoma(long idSintoma, String sintoma) {
		super();
		this.idSintoma = idSintoma;
		this.sintoma = sintoma;
	}
	public long getIdSintoma() {
		return idSintoma;
	}
	public void setIdSintoma(long idSintoma) {
		this.idSintoma = idSintoma;
	}
	public String getSintoma() {
		return sintoma;
	}
	public void setSintoma(String sintoma) {
		this.sintoma = sintoma;
	}
}
