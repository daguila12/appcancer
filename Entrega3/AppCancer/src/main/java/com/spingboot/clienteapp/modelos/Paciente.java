package com.spingboot.clienteapp.modelos;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@PrimaryKeyJoinColumn(name="IdUsuario")
public class Paciente extends Usuario {
      
	  public long IdTratamiento;
      public long IdEtapa;
      public long IdModerador;
      
      
      public Paciente() {};
      
      public Paciente(long idTratamiento, long idEtapa, long idModerador) {
		super();
		IdTratamiento = idTratamiento;
		IdEtapa = idEtapa;
		IdModerador = idModerador;
	}


	public Paciente(long idUsuario, String correo, String nombreUsuario, String contrasena, long idTratamiento, long idEtapa, long idModerador) {
		super( idUsuario, correo, nombreUsuario, contrasena);
		IdTratamiento = idTratamiento;
		IdEtapa = idEtapa;
		IdModerador = idModerador;
      }
      
      
      public long getIdTratamiento() {
		return IdTratamiento;
      }
      
      
      public void setIdTratamiento(long idTratamiento) {
		IdTratamiento = idTratamiento;
      }
      
      
      public long getIdEtapa() {
		return IdEtapa;
      }
      
      
      public void setIdEtapa(long idEtapa) {
		IdEtapa = idEtapa;
      }
      
      
      public long getIdModerador() {
		return IdModerador;
      }
      
      
      public void setIdModerador(long idModerador) {
		IdModerador = idModerador;
      }
      
      
}
