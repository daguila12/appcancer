package com.spingboot.clienteapp.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity 
@Table(name="Etapa_Sintoma") 
public class Etapa_Sintoma {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long IdEtapa_Sintoma;
	private long IdEtapa;
 	private long IdSintoma;
 	  
 	public Etapa_Sintoma() {};  
 	  
	public Etapa_Sintoma(long idEtapa, long idSintoma) {
		super();
		IdEtapa = idEtapa;
		IdSintoma = idSintoma;
	}
	public long getIdEtapa() {
		return IdEtapa;
	}
	public void setIdEtapa(long idEtapa) {
		IdEtapa = idEtapa;
	}
	public long getIdSintoma() {
		return IdSintoma;
	}
	public void setIdSintoma(long idSintoma) {
		IdSintoma = idSintoma;
	}
 	  
 	  
}
