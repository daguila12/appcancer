package com.spingboot.clienteapp.modelos;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "Reporte")
public class Reporte {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long IdReporte;
//	private Date fecha;
	private long IdPaciente;
	
	@Column(name= "ClasificacionProfesional")
	private long ClasificacionProfesional = -1;
	@Column(name= "ComentarioProfesional", columnDefinition = "varchar(100) default null")
	private String ComentarioProfesional;
	
	public Reporte() {};
	
	public Reporte(long idReporte, Date fecha, long idPaciente) {
		super();
		IdReporte = idReporte;
//		this.fecha = fecha;
		IdPaciente = idPaciente;
	}
	public long getIdReporte() {
		return IdReporte;
	}
	public void setIdReporte(long idReporte) {
		IdReporte = idReporte;
	}
/*	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}*/
	public long getIdPaciente() {
		return IdPaciente;
	}
	public void setIdPaciente(long idPaciente) {
		IdPaciente = idPaciente;
	}
	public long getClasificacionProfesional() {
		return ClasificacionProfesional;
	}
	public void setClasificacionProfesional(int cp) {
		this.ClasificacionProfesional = cp;
	}
	public String getComentarioProfesional() {
		return ComentarioProfesional;
	}
	public void setComentarioProfesional(String cp) {
		this.ComentarioProfesional = cp;
	}
}
