PGDMP     /                    x         	   AppCancer    12.3    12.3 "    >           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            ?           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            @           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            A           1262    16439 	   AppCancer    DATABASE     �   CREATE DATABASE "AppCancer" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE "AppCancer";
                postgres    false            �            1259    24827    etapa    TABLE     �   CREATE TABLE public.etapa (
    id_etapa bigint NOT NULL,
    id_tratamiento bigint NOT NULL,
    nombre_etapa character varying(255)
);
    DROP TABLE public.etapa;
       public         heap    postgres    false            �            1259    24837    etapa_sintoma    TABLE     �   CREATE TABLE public.etapa_sintoma (
    id_etapa_sintoma bigint NOT NULL,
    id_etapa bigint NOT NULL,
    id_sintoma bigint NOT NULL
);
 !   DROP TABLE public.etapa_sintoma;
       public         heap    postgres    false            �            1259    16456    hibernate_sequence    SEQUENCE     {   CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public          postgres    false            �            1259    24798    paciente    TABLE     �   CREATE TABLE public.paciente (
    id_etapa bigint NOT NULL,
    id_moderador bigint NOT NULL,
    id_tratamiento bigint NOT NULL,
    id_usuario bigint NOT NULL
);
    DROP TABLE public.paciente;
       public         heap    postgres    false            �            1259    24789    pregunta    TABLE     �   CREATE TABLE public.pregunta (
    idpregunta bigint NOT NULL,
    nombre_completo character varying(255),
    pregunta character varying(255),
    respuesta character varying(255),
    numero bigint
);
    DROP TABLE public.pregunta;
       public         heap    postgres    false            �            1259    24657    reporte    TABLE     J  CREATE TABLE public.reporte (
    id_reporte bigint NOT NULL,
    id_paciente bigint NOT NULL,
    fecha date,
    id_moderador bigint NOT NULL,
    id_etapa bigint NOT NULL,
    id bigint NOT NULL,
    severidad integer NOT NULL,
    sintoma character varying(255),
    id_dato bigint NOT NULL,
    id_usuario bigint NOT NULL
);
    DROP TABLE public.reporte;
       public         heap    postgres    false            �            1259    24842    reporte_sintoma    TABLE     �   CREATE TABLE public.reporte_sintoma (
    id bigint NOT NULL,
    id_reporte bigint NOT NULL,
    sintoma character varying(255),
    id_sintoma bigint NOT NULL,
    severidad integer NOT NULL
);
 #   DROP TABLE public.reporte_sintoma;
       public         heap    postgres    false            �            1259    24632    sintoma    TABLE     d   CREATE TABLE public.sintoma (
    id_sintoma bigint NOT NULL,
    sintoma character varying(255)
);
    DROP TABLE public.sintoma;
       public         heap    postgres    false            �            1259    24816    tratamiento    TABLE     q   CREATE TABLE public.tratamiento (
    id_tratamiento bigint NOT NULL,
    nombre_etapa character varying(255)
);
    DROP TABLE public.tratamiento;
       public         heap    postgres    false            �            1259    24803    usuario    TABLE     �   CREATE TABLE public.usuario (
    id_usuario bigint NOT NULL,
    nombre_usuario character varying(255),
    contrasena character varying(255),
    correo character varying(255)
);
    DROP TABLE public.usuario;
       public         heap    postgres    false            9          0    24827    etapa 
   TABLE DATA           G   COPY public.etapa (id_etapa, id_tratamiento, nombre_etapa) FROM stdin;
    public          postgres    false    209   �%       :          0    24837    etapa_sintoma 
   TABLE DATA           O   COPY public.etapa_sintoma (id_etapa_sintoma, id_etapa, id_sintoma) FROM stdin;
    public          postgres    false    210   !&       6          0    24798    paciente 
   TABLE DATA           V   COPY public.paciente (id_etapa, id_moderador, id_tratamiento, id_usuario) FROM stdin;
    public          postgres    false    206   M&       5          0    24789    pregunta 
   TABLE DATA           \   COPY public.pregunta (idpregunta, nombre_completo, pregunta, respuesta, numero) FROM stdin;
    public          postgres    false    205   |&       4          0    24657    reporte 
   TABLE DATA           �   COPY public.reporte (id_reporte, id_paciente, fecha, id_moderador, id_etapa, id, severidad, sintoma, id_dato, id_usuario) FROM stdin;
    public          postgres    false    204   _'       ;          0    24842    reporte_sintoma 
   TABLE DATA           Y   COPY public.reporte_sintoma (id, id_reporte, sintoma, id_sintoma, severidad) FROM stdin;
    public          postgres    false    211   |'       3          0    24632    sintoma 
   TABLE DATA           6   COPY public.sintoma (id_sintoma, sintoma) FROM stdin;
    public          postgres    false    203   �'       8          0    24816    tratamiento 
   TABLE DATA           C   COPY public.tratamiento (id_tratamiento, nombre_etapa) FROM stdin;
    public          postgres    false    208   �'       7          0    24803    usuario 
   TABLE DATA           Q   COPY public.usuario (id_usuario, nombre_usuario, contrasena, correo) FROM stdin;
    public          postgres    false    207   �'       B           0    0    hibernate_sequence    SEQUENCE SET     B   SELECT pg_catalog.setval('public.hibernate_sequence', 204, true);
          public          postgres    false    202            �
           2606    24831    etapa etapa_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.etapa
    ADD CONSTRAINT etapa_pkey PRIMARY KEY (id_etapa);
 :   ALTER TABLE ONLY public.etapa DROP CONSTRAINT etapa_pkey;
       public            postgres    false    209            �
           2606    24841     etapa_sintoma etapa_sintoma_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.etapa_sintoma
    ADD CONSTRAINT etapa_sintoma_pkey PRIMARY KEY (id_etapa_sintoma);
 J   ALTER TABLE ONLY public.etapa_sintoma DROP CONSTRAINT etapa_sintoma_pkey;
       public            postgres    false    210            �
           2606    24802    paciente paciente_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.paciente
    ADD CONSTRAINT paciente_pkey PRIMARY KEY (id_usuario);
 @   ALTER TABLE ONLY public.paciente DROP CONSTRAINT paciente_pkey;
       public            postgres    false    206            �
           2606    24796    pregunta pregunta_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.pregunta
    ADD CONSTRAINT pregunta_pkey PRIMARY KEY (idpregunta);
 @   ALTER TABLE ONLY public.pregunta DROP CONSTRAINT pregunta_pkey;
       public            postgres    false    205            �
           2606    24661    reporte reporte_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.reporte
    ADD CONSTRAINT reporte_pkey PRIMARY KEY (id_reporte);
 >   ALTER TABLE ONLY public.reporte DROP CONSTRAINT reporte_pkey;
       public            postgres    false    204            �
           2606    24846 $   reporte_sintoma reporte_sintoma_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.reporte_sintoma
    ADD CONSTRAINT reporte_sintoma_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.reporte_sintoma DROP CONSTRAINT reporte_sintoma_pkey;
       public            postgres    false    211            �
           2606    24636    sintoma sintoma_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.sintoma
    ADD CONSTRAINT sintoma_pkey PRIMARY KEY (id_sintoma);
 >   ALTER TABLE ONLY public.sintoma DROP CONSTRAINT sintoma_pkey;
       public            postgres    false    203            �
           2606    24820    tratamiento tratamiento_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.tratamiento
    ADD CONSTRAINT tratamiento_pkey PRIMARY KEY (id_tratamiento);
 F   ALTER TABLE ONLY public.tratamiento DROP CONSTRAINT tratamiento_pkey;
       public            postgres    false    208            �
           2606    24810    usuario usuario_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    207            �
           2606    24811 $   paciente fk1vx4fcl7eb0wbyvff1184dr0m    FK CONSTRAINT     �   ALTER TABLE ONLY public.paciente
    ADD CONSTRAINT fk1vx4fcl7eb0wbyvff1184dr0m FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);
 N   ALTER TABLE ONLY public.paciente DROP CONSTRAINT fk1vx4fcl7eb0wbyvff1184dr0m;
       public          postgres    false    2730    207    206            9      x�3�4����L�L̉������ 3D�      :      x�3�4�4�2�F\�@Ҙ+F��� !��      6      x�3�4B#.C(��2����b���� ���      5   �   x�=OKjC1\˧�	B��@�
]J�"$Yv��"l��g�[���bу�m�F��h&8s�E�g��ʗ!��C�V������ !L#6���apP용b�J�;eM�"f���3�=�4)�`����oj�Iz��֭wO����f��Ns �Eѫ��)�`m�5�h�Q3W]��3D��6��#樦۸������E��WMԌt+��Y�b'      4      x������ � �      ;      x������ � �      3   4   x�3����,�/�2�t���/RHLJ��%�pCERR��R��b���� ���      8      x�3�H����� ��      7   F   x��1�0g�*I���j:P%,��&�N�� (�u|A#r�Ņ7M�=-�J�����{^"� }     